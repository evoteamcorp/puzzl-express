import {Request} from "express";
import {Response} from "express";
import {NextFunction} from "express";
/**
 * Created by acidspike on 13/06/2016.
 */
export interface RouteDef {
    controller: string;
    moduleName: string;
    action?: string;
    view?: string;
    needAuth?: boolean;
    needAuthRedirect?: string;
    auth?: string;
    authOptions?: any;
}

export interface PathDef {
    path: string;
    moduleName: string;
}

export interface LocalDef {
    name: string;
    local: any;
}

export interface WaitingRouteDictionary {
    [index: string]: WaitingRoute;
}

export interface WaitingRoute {
    [index: string]: (req: Request, res: Response, next: NextFunction) => void;
}

export interface ErrorMessageFallbackDictionary {
    [index: string]: string;
}