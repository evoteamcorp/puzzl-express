/**
 * Created by acidspike on 13/06/2016.
 */
///<reference path="../typings/index.d.ts" />
///<reference path="../node_modules/puzzl-core/manual_typings/global.d.ts" />
///<reference path="../node_modules/puzzl-file-watcher/manual_typings/global.d.ts" />

declare namespace Express {
    export interface Request {
        t(key: string, options?: any): string;
    }

    export interface Response {
        append(headerName: string, headerValue: string): void;
    }
}
