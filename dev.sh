#!/usr/bin/env bash

if [[ $1 == "yes" ]]; then
    echo "Add Puzzl-Core"
    ln -s ../Core ./node_modules/puzzl-core
    echo "Add Puzzl-File-Watcher"
    ln -s ../File-Watcher ./node_modules/puzzl-file-watcher
else
    echo "Remove Puzzl-Core"
    rm ./node_modules/puzzl-core
    echo "Remove Puzzl-File-Watcher"
    rm ./node_modules/puzzl-file-watcher
fi