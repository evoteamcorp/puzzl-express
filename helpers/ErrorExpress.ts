/**
 * Created by acidspike on 14/06/2016.
 */
import {Request} from "express";
import {ErrorMessageFallbackDictionary} from "../manual_typings/built_in";

export class ErrorExpress extends Error {
    private static _errorsMessageFallback: ErrorMessageFallbackDictionary = {
        "Auth.Error.NeedAuthenticationOrAuthorization": "You need to be authenticated / authorized to access this resource",
        "Http.Error.ScaffoldingNotImplementedYet": "Scaffolding not implemented yet",
        "Http.Error.NoController": "No controller {{controller}} registered",
        "Http.Error.NoControllerAction": "The controller {{controller}} does not have action named: {{action}}"
    };

    public statusCode: Number;

    constructor(req: Request, message?: string, datas?: any) {
        if (typeof req.t === "function") {
            message = req.t(message);
        } else if (ErrorExpress._errorsMessageFallback.hasOwnProperty(message)) {
            message = ErrorExpress._errorsMessageFallback[message];
            Object.keys(datas).forEach((data) => {
                if (datas.hasOwnProperty(data)) {
                    let regex = new RegExp("{{" + data + "}}", "gi");
                    message = message.replace(regex, datas[data]);
                }
            });
        }
        super(message);
    }
}