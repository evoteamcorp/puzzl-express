/**
 * Created by acidspike on 13/06/2016.
 */
import {Request} from "express";
import {Response} from "express";
import {NextFunction} from "express";
import {CsrfUtil} from "../utils/CsrfUtil";
import {FlashUtil} from "../utils/FlashUtil";
import {UrlUtil} from "../utils/UrlUtil";

export = {
    blueprint      : true,
    middlewares    : {
        compression           : true,
        "cookie-parser"       : {
            args: [
                "8EspLw7za3Dfsyp"
            ]
        },
        "cookie-session"      : {
            args: [
                "8EspLw7za3Dfsyp"
            ]
        },
        csurf                 : {
            after          : "method-override",
            args           : [
                {
                    keyName: "formProtect",
                    value  : (req: Request): string => {
                        let key = Puzzl.getInjector().get("ConfigsCore").get("Express").middlewares.csurf.args[0].keyName;
                        if (req.body[key]) {
                            return req.body[key];
                        } else if (req.query[key]) {
                            return req.query[key];
                        } else if (req.headers["x-" + key + "-token"]) {
                            return req.query["x-" + key + "-token"];
                        } else {
                            return "";
                        }
                    }
                }
            ],
            extraMiddleware: (req: Request, res: Response, next: NextFunction): void => {
                res.locals.Csrf =
                    new (
                        Puzzl.getInjector().get("CsrfUtil") as typeof CsrfUtil
                    )(req, Puzzl.getInjector().get("ConfigsCore").get("Express").middlewares.csurf.args[0].keyName);
                next();
            }
        },
        "express-session"     : {
            after: "cookie-parser",
            args : [
                {
                    name             : "puzzl.sid",
                    resave           : true,
                    saveUninitialized: true,
                    secret           : "8EspLw7za3Dfsyp"
                }
            ]
        },
        "express-uncapitalize": true,
        helmet                : {
            subs: {
                contentSecurityPolicy: false,
                dnsPrefetchControl   : true,
                frameguard           : {
                    args: ["DENY"]
                },
                hidePoweredBy        : {
                    args: [
                        {
                            setTo: "Puzzl Framework"
                        }
                    ]
                },
                hpkp                 : false,
                hsts                 : {
                    args: [
                        {
                            includeSubdomains: true,
                            maxAge           : 7776000000,
                            preload          : true

                        }
                    ]
                },
                ieNoOpen             : true,
                noCache              : false,
                noSniff              : true,
                xssFilter            : true
            }
        },
        "method-override"     : {
            args: ["X-HTTP-Method-Override"]
        },
        morgan                : {
            args: [
                "combined",
                {
                    stream: {
                        write: (log: string): void => {
                            Puzzl.getInjector().get("LoggerCore").info("[REQUEST]", log);
                        }
                    }
                }
            ]
        },
        "response-time"       : true,
        "serve-favicon"       : {
            args: [
                Puzzl.getInjector().get("PathsUtil").getPiecePath("Express", "static/favicon.ico")
            ]
        },
        "serve-static"        : {
            args: [
                {
                    dotfiles: "ignore",
                    maxAge  : 3600
                }
            ],
            last: true
        },
        "connect-flash"       : {
            after: "express-session",
            extraMiddleware: (req: Request, res: Response, next: NextFunction): void => {
                res.locals.Flash = new (Puzzl.getInjector().get("FlashUtil") as typeof FlashUtil)(req);
                next();
            }
        },
        "url-util": (req: Request, res: Response, next: NextFunction): void => {
            res.locals.Url = new (Puzzl.getInjector().get("UrlUtil") as typeof UrlUtil)(req);
            next();
        }
    },
    paths          : {
        blueprint: "models",
        "public" : "public",
        views    : "views"
    },
    templateEngines: [
        "atpl",
        "dot",
        "dust",
        "eco",
        "ect",
        "ejs",
        "haml",
        "hamlet",
        "handlebars",
        "hogan",
        "htmling",
        "jade",
        "jazz",
        "jqtpl",
        "just",
        "liquid",
        "liquor",
        "mote",
        "mustache",
        "nunjucks",
        "qejs",
        "ractive",
        "react",
        "slm",
        "swig",
        "templayed",
        "toffee",
        "twig",
        "vash",
        "walrus",
        "whiskers"
    ]
};