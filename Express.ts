import {Express} from "express";
import {
    IPiece,
    PieceManager,
    EventManager,
    PathsUtil,
    Injectable,
    Injector,
    Piece,
    InjectableDefinition,
    InjectableOptions,
    ConfigsCore,
    LoggerCore,
    PieceOptions,
    FileUtil,
    LoaderCore,
    PackageJsonCore
} from "puzzl-core/Exports";
import {LocalDef} from "./manual_typings/built_in";
import {ControllerManager} from "./managers/ControllerManager";

/**
 * Main class of express module for modular
 */

@Injector()
@Piece(
    {
        asStart: true
    } as PieceOptions
)
@Injectable(
    {
        injectables: [
            {
                category: "Manager",
                path    : __dirname + "/managers"
            },
            {
                category: "Util",
                path    : __dirname + "/utils"
            }
        ] as Array<InjectableDefinition>
    } as InjectableOptions
)
export class ExpressPiece implements IPiece {
    public promise: Promise<void>;

    private _express: Express             = null;
    private _server: any                  = null;
    private _consolidate: any             = null;
    private _moduleName: string           = "Express";
    private _viewPaths: Array<string>     = [];
    private _staticPaths: Array<string>   = [];
    private _middlewares: Array<Function> = [];
    private _locals: Array<LocalDef>      = [];
    private _sockets: any                 = {};

    /**
     * Require express package on module instantiation
     */
    constructor(
        instanciated: (value?: any) => void,
        error: (error: any) => void,
        private _pieceManager: PieceManager,
        private _eventManager: EventManager,
        private _pathsUtil: PathsUtil,
        private _configsCore: ConfigsCore,
        private _logger: LoggerCore,
        private _controllerManager: ControllerManager,
        private _fileUtil: FileUtil,
        private _loaderCore: LoaderCore,
        private _packageJsonCore: PackageJsonCore
    ) {
        this._express     = require("express")();
        this._consolidate = require("consolidate");
        this._configsCore.loadPieceConfigs(this._moduleName).then(instanciated).catch(error);
    }

    /**
     * Initialize express
     */
    public initialize(initialized: (value?: any) => void, error: (error?: any) => void): void {
        if (!this._pieceManager.isLoaded(this._moduleName)) {
            this._eventManager.on(
                "configs:Routes:updated", () => {
                    this.restart();
                }
            );
        }
        this.addViewPath(this._pathsUtil.getApp(this._configsCore.get("Express").paths.views));
        this.addStaticPath(this._pathsUtil.getApp(this._configsCore.get("Express").paths.public));

        this._initializeExpress().then(initialized).catch(error);
    }

    /**
     * Restart express
     * @returns {Promise<any>}
     */
    public restart(): Promise<any> {
        let promiseRestartExpress = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            this._logger.debug("Restart express");
            this._server.close(
                () => {
                    this._express     = require("express")();
                    this._middlewares = [];
                    this._staticPaths = [];
                    this._viewPaths   = [];
                    this._sockets     = {};
                    let p             = new Promise(
                        (resolveInit: (value?: any) => void, rejectInit: (error: any) => void) => {
                            this.initialize(resolveInit, rejectInit);
                        }
                    );
                    p.then(
                        () => {
                            return this.start();
                        }
                    ).then(resolve).catch(reject);
                }
            );

            for (let socketId in this._sockets) {
                if (this._sockets.hasOwnProperty(socketId)) {
                    this._logger.debug("socket", socketId, "destroyed");
                    this._sockets[socketId].destroy();
                }
            }
        };

        return new Promise(promiseRestartExpress);
    }

    /**
     * Return Module path
     */
    public getPath(): string {
        return __dirname;
    }

    /**
     * Start Express nothing to do here
     * @return {Promise<T>}
     * @private
     */
    public start(): Promise<any> {
        let promiseExpressLoad = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            try {
                this._controllerManager.setExpress(this._express);

                this._loadMiddlewares().then(
                    () => {
                        return this._loadLocals();
                    }
                ).then(
                    () => {
                        return this._controllerManager.load();
                    }
                ).then(
                    () => {
                        return this._launchExpress();
                    }
                ).then(resolve).catch(reject);
            } catch (e) {
                reject(e);
            }
        };

        return new Promise(promiseExpressLoad);
    }

    /**
     * Return express instance
     * @returns {Express}
     */
    public getExpress(): Express {
        return this._express;
    }

    /**
     * Add a view path
     * @param path
     */
    public addViewPath(path: string): void {
        this._viewPaths.push(path);
    }

    /**
     * Add a static path
     * @param path
     */
    public addStaticPath(path: string): void {
        this._staticPaths.push(path);
    }

    /**
     * Add a middleware to load
     * @param middleware
     */
    public addMiddleware(middleware: Function): void {
        this._middlewares.push(middleware);
    }

    /**
     * Add local to load
     * @param local
     */
    public addLocal(local: LocalDef): void {
        this._locals.push(local);
    }

    /**
     * Initialize express
     * @returns {Promise}
     * @private
     */
    private _initializeExpress(): Promise<any> {
        let promiseInitializeExpress = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            return this._registerTemplateEngines()
                       .then(resolve)
                       .catch(reject);
        };

        return new Promise(promiseInitializeExpress);
    }

    /**
     * Register template engines
     * @returns {Promise}
     * @private
     */
    private _registerTemplateEngines(): Promise<any> {
        let promiseRegisterTemplateEngines = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            try {
                let tpls = this._configsCore.get(this._moduleName).templateEngines;
                tpls.forEach(
                    (tpl: string) => {
                        this._express.engine(tpl, this._consolidate[tpl]);
                    }
                );

                resolve();
            } catch (e) {
                reject(e);
            }
        };

        return new Promise(promiseRegisterTemplateEngines);
    }

    /**
     * Load middlewares
     * @returns {Promise}
     * @private
     */
    private _loadMiddlewares(): Promise<any> {
        let promiseLoadMiddlewares = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            let configs = this._configsCore.get(this._moduleName).middlewares;

            try {
                this._logger.debug("Register views paths");
                this._express.set("views", this._viewPaths);

                this._logger.debug("Load skipper middleware");
                this._express.use(require("skipper")());

                let loadMiddlewares = (givenConfigs: any, module?: any): Promise<any> => {
                    let middlewares = this._getOrderedMiddlewares(givenConfigs);

                    return Promise.each(
                        middlewares, (middlewareName: string) => {
                            return new Promise(
                                (resolveLoad: (value?: any) => void, rejectLoad: (error: any) => void) => {
                                    if (givenConfigs.hasOwnProperty(middlewareName) && givenConfigs[middlewareName] !== false) {
                                        this._logger.debug("Load middleware:", middlewareName);
                                        if (middlewareName === "app") {
                                            this._loadAppMiddlewares().then(resolveLoad).catch(rejectLoad);
                                        } else if (typeof middlewareName === "function") {
                                            this._express.use(middlewareName);
                                        } else {
                                            let confs = givenConfigs[middlewareName];
                                            let mdl   = (
                                                            module
                                                        ) ? module[middlewareName] : require(middlewareName);
                                            if (confs.subs) {
                                                loadMiddlewares(confs.subs, mdl).then(resolveLoad).catch(rejectLoad);
                                            } else {
                                                try {
                                                    if (middlewareName === "serve-static") {
                                                        this._staticPaths.forEach(
                                                            (path: string) => {
                                                                this._express.use(mdl(path, confs.args[0]));
                                                            }
                                                        );
                                                    } else {
                                                        if (confs.args) {
                                                            let args = this._updateArgs(confs.args);
                                                            mdl      = mdl.apply(mdl, args);
                                                        } else {
                                                            mdl = mdl();
                                                        }

                                                        if (!confs.noUse) {
                                                            this._express.use(mdl);
                                                        }

                                                        if (confs.extraMiddleware && typeof confs.extraMiddleware === "function") {
                                                            this._express.use(confs.extraMiddleware);
                                                        }
                                                    }

                                                    resolveLoad();
                                                } catch (e) {
                                                    rejectLoad(e);
                                                }
                                            }
                                        }
                                    } else {
                                        this._logger.debug(middlewareName, "middleware disabled");
                                        resolveLoad();
                                    }
                                }
                            );
                        }
                    );
                };

                loadMiddlewares(configs).then(resolve).catch(reject);

            } catch (e) {
                reject(e);
            }
        };

        return new Promise(promiseLoadMiddlewares);
    }

    /**
     * Load application middlewares
     * @returns {PromiseLike<TResult>}
     * @private
     */
    private _loadAppMiddlewares(): Promise<any> {
        let middlewaresPath = this._pathsUtil.getApp("middlewares");
        return this._fileUtil.dirExist(middlewaresPath).then(
            (result: boolean) => {
                return new Promise(
                    (resolve: (value?: any) => void, reject: (error: any) => void) => {
                        if (result) {
                            this._requireAppMiddlewares(middlewaresPath).then(resolve).catch(reject);
                        } else {
                            resolve();
                        }
                    }
                );
            }
        ).then(
            () => {
                return new Promise(
                    (resolve: (value?: any) => void, reject: (error: any) => void) => {
                        try {
                            if (this._middlewares.length) {
                                this._middlewares.forEach(
                                    (middleware: Function) => {
                                        this._express.use(middleware());
                                    }
                                );
                            }
                        } catch (e) {
                            reject(e);
                        }

                        resolve();
                    }
                );
            }
        );
    }

    /**
     * Load locals in express
     * @private
     */
    private _loadLocals(): Promise<any> {
        let promiseLoadLocal = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            this._logger.debug("Load locals in express...");
            try {
                this._express.locals.package =
                    this._packageJsonCore.getApp();
                this._locals.forEach(
                    (local: LocalDef) => {
                        this._express.locals[local.name] = local.local;
                    }
                );

                resolve();
            } catch (e) {
                reject(e);
            }
        };

        return new Promise(promiseLoadLocal);
    }

    /**
     * Launch express
     * @returns {Promise}
     * @private
     */
    private _launchExpress(): Promise<any> {
        let promiseLaunchExpress = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            try {
                let config = this._configsCore.get(this._moduleName);
                this._logger.debug("Attempt to start Express");
                this._server     = this._express.listen(
                    config.port, () => {
                        this._logger.debug("Express started and listening on port :", config.port);
                        resolve();
                    }
                );
                let nextSocketId = 0;
                this._server.on(
                    "connection", (socket: any) => {
                        let soId            = nextSocketId++;
                        this._sockets[soId] = socket;

                        socket.on(
                            "close", () => {
                                try {
                                    delete this._sockets[soId];
                                } catch (e) {
                                }
                            }
                        );
                    }
                );
            } catch (e) {
                reject(e);
            }
        };

        return new Promise(promiseLaunchExpress);
    }

    /**
     * Require application middlewares
     * @param middlewaresPath
     * @returns {any}
     * @private
     */
    private _requireAppMiddlewares(middlewaresPath: string): Promise<any> {
        return this._fileUtil.getFilesInDir(middlewaresPath)
                   .then(
                       (files: Array<string>) => {
                           return Promise.each(
                               files, (file: string) => {
                                   return this._loaderCore.requireNoPiece(
                                       middlewaresPath + "/" + file, (name: string, middleware: any) => {
                                           return new Promise(
                                               (resolveRequire: (value?: any) => void, rejectRequire: (error?: any) => void) => {
                                                   try {
                                                       this._express.use(middleware());
                                                       resolveRequire();
                                                   } catch (e) {
                                                       rejectRequire(e);
                                                   }
                                               }
                                           );
                                       }
                                   );
                               }
                           );
                       }
                   );
    }

    /**
     * Update args for placeholder replacement
     * @param args
     * @returns {Array}
     * @private
     */
    private _updateArgs(args: Array<any>): Array<any> {
        let nArgs = [];

        args.forEach(
            (arg: any) => {
                if (arg === "express") {
                    nArgs.push(this.getExpress());
                } else {
                    nArgs.push(arg);
                }
            }
        );

        return nArgs;
    }

    /**
     * Return ordered middlewares
     * @param givenConfigs
     * @private
     */
    private _getOrderedMiddlewares(givenConfigs: any): Array<string> {
        let afters  = {};
        let lasts   = [];
        let befores = {};
        let firsts  = [];

        let ordered = [];

        Object.keys(givenConfigs).forEach(
            (key: string) => {
                if (givenConfigs[key].first) {
                    firsts.push(key);
                } else if (givenConfigs[key].last) {
                    lasts.push(key);
                } else if (givenConfigs[key].after) {
                    afters[givenConfigs[key].after] = afters[givenConfigs[key].after] || [];
                    afters[givenConfigs[key].after].push(key);
                } else if (givenConfigs[key].before) {
                    befores[givenConfigs[key].before] = befores[givenConfigs[key].before] || [];
                    befores[givenConfigs[key].before].push(key);
                }
            }
        );

        let inject = (keys: Array<string>, lastMode: boolean = false) => {
            keys.forEach(
                (key: string) => {
                    if (ordered.indexOf(key) ===
                        -1 &&
                        (
                        lasts.indexOf(key) === -1 || lastMode
                        )) {
                        if (befores.hasOwnProperty(key)) {
                            befores[key].forEach(
                                (beforeKey: string) => {
                                    ordered.push(beforeKey);
                                }
                            );
                        }

                        ordered.push(key);

                        if (afters.hasOwnProperty(key)) {
                            afters[key].forEach(
                                (afterKey: string) => {
                                    ordered.push(afterKey);
                                }
                            );
                        }
                    }
                }
            );
        };

        inject(firsts);
        inject(Object.keys(givenConfigs));
        inject(lasts);

        return ordered;
    }
}