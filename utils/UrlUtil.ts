import {Request} from "express";
import {IUtil, LoggerCore, MetaObject, Injector} from "puzzl-core/Exports";

/**
 * Static class for managing Url
 */
@MetaObject()
@Injector()
export class UrlUtil implements IUtil {

    constructor(private _req: Request, private _logger?: LoggerCore) {
    }

    /**
     * Return if given url is the same of req, if no req provided use the instance req
     * @param url
     * @param req
     * @returns {boolean}
     */
    public isSameUrl(url: string, req?: Request): boolean {
        try {
            req = req || this._req;
            return req.path === url;
        } catch (e) {
            this._logger.error(e);
            return false;
        }
    }

    /**
     * Return if req path start with given url
     * @param url
     * @param req
     * @returns {boolean}
     */
    public startWith(url: string, req?: Request): boolean {
        try {
            req = req || this._req;
            return (
                new RegExp("^" + url)
            ).test(req.path);
        } catch (e) {
            this._logger.error(e);
            return false;
        }
    }
}