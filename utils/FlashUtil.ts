import {Request} from "express";
import {IUtil, MetaObject, LoggerCore, Injector} from "puzzl-core/Exports";

/**
 * Static class for managing Flash
 */
@MetaObject()
@Injector()
export class FlashUtil implements IUtil {

    constructor(private _req: Request, private _logger?: LoggerCore) {
    }

    /**
     * Return a this flash message from req
     * @param scope
     * @param req
     * @returns {any}
     */
    public getFlash(scope: string, req?: Request): Array<string> {
        try {
            req = req || this._req;
            return req.flash(scope);
        } catch (e) {
            this._logger.error(e);
            return [];
        }
    }
}