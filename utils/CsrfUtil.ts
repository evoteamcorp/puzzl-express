import {Request} from "express";
import {IUtil, MetaObject, Injector, LoggerCore, ConfigsCore} from "puzzl-core/Exports";

/**
 * Static class for managing Csrf
 */
@MetaObject()
@Injector()

export class CsrfUtil implements IUtil {

    constructor(private _req?: Request, private _csrfName?: string, private _logger?: LoggerCore, private _configsCore?: ConfigsCore) {
    }

    /**
     * Return a csrfToken from req
     * @param req
     * @returns {string}
     */
    public getCsrfToken(req?: Request): string {
        try {
            req = req || this._req;
            return req.csrfToken();
        } catch (e) {
            this._logger.error(e);
            return "";
        }
    }

    /**
     * Return the keyName for the csrf protection
     * @returns {any}
     */
    public getCsrfName(): string {
        try {
            return this._csrfName || this._configsCore.get("Express").middlewares.csurf.args[0].keyName;
        } catch (e) {
            this._logger.error(e);
            return "";
        }
    }
}