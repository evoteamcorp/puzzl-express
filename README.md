# Puzzl-Express #

Puzzl-Express est une pièce du framework [Puzzl](https://bitbucket.org/evoteamcorp/puzzl-core), cette pièce permet de facilement déclarer
 un serveur http.

### Informations de la pièce ###

* **Nom**: Puzzl-Express
* **Version**: 0.0.1
* **Date de release**: N/A

### Pré-requis ###

* **Versions de NodeJS / iojs**
* * **NodeJS**: >= 6
* * **iojs**: N/A
* **Dépendances**
* * **express**: ^4.13.4
* * **consolidate**: ^0.14.1
* * **skipper**: ^0.5.9
* * **compression**: ^1.6.2
* * **cookie-parser**: ^1.4.3
* * **cookie-session**: ^2.0.0-alpha.1
* * **csurf**: ^1.9.0
* * **express-session**: ^1.13.0
* * **express-uncapitalize**: ^1.0.0
* * **method-override**: ^2.3.6
* * **morgan**: ^1.7.0
* * **response-time**: ^2.3.1
* * **serve-favicon**: ^2.3.0
* * **serve-static**: ^1.11.1
* * **connect-flash**: ^0.1.1
* **Dépendances Optionnelles**
* * **puzzl-file-watcher**: ^0.0.1

### Comment contribuer ###

* Ecrire des tests unitaires, car je n'ai aucune idée de comment m'y prendre T_T
* Ne pas hésiter à proposer des améliorations
* Ouvrir un ticket en cas de bug

### Qui est derrière Puzzl-Express et comment me contacter ###

* Anthony Briand (acidspike) <ace130210@gmail.com>