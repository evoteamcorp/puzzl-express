/**
 * Created by acidspike on 14/06/2016.
 */
import {IManager, LoggerCore, Instantiate, MetaObject, Injector} from "puzzl-core/Exports";
import {Request} from "express";
import {Response} from "express";
import {NextFunction} from "express";
import {RouteDef} from "../manual_typings/built_in";

@Instantiate()
@MetaObject()
@Injector()
export class AuthManager implements IManager {

    private _authCheckers: Array<Function>              = [];
    private _authenticationMiddlewares: Array<Function> = [];

    constructor(private _logger: LoggerCore) {
    }

    /**
     * Initialize AuthManager
     * @returns {Promise}
     */
    public initialize(): Promise<any> {
        return new Promise(
            (resolve: (value?: any) => void) => {
                resolve();
            }
        );
    }

    /**
     * Add an auth checker
     * @param checker
     */
    public addAuthChecker(checker: Function): void {
        this._logger.debug("Add auth checker:", checker);
        this._authCheckers.push(checker);
    }

    /**
     * Add an authentication middleware
     * @param authenticator
     */
    public addAuthenticationMiddleware(authenticator: Function): void {
        this._logger.debug("Add authenticator middleware:", authenticator);
        this._authenticationMiddlewares.push(authenticator);
    }

    /**
     * Check if the request is authorized
     * @param req
     * @param res
     * @param next
     * @param def
     * @returns {Promise}
     */
    public checkIfAuthorized(req: Request, res: Response, next: NextFunction, def?: RouteDef): Promise<boolean> {
        let authorized: boolean = true;
        return new Promise((resolve: (value: boolean) => void, reject: (error: any) => void) => {
            Promise.each(this._authCheckers, (authChecker: Function) => {
                let result = authChecker(req, res, next, def);

                return result.then((authorizedResult: boolean) => {
                    if (authorized) {
                        authorized = authorizedResult;
                    }
                });
            }).then(() => {
                resolve(authorized);
            }).catch(reject);
        });
    }

    /**
     * Check if the request is an authentication request
     * @param req
     * @param res
     * @param next
     * @param def
     * @returns {Promise}
     */
    public checkIfAuthentication(req: Request, res: Response, next: NextFunction, def: RouteDef): Promise<boolean> {
        let authenticated: boolean = false;

        return new Promise((resolve: (value: boolean) => void, reject: (error: any) => void) => {
            Promise.each(this._authenticationMiddlewares, (authenticator: Function) => {
                return new Promise((resolveAuthenticator: () => void, rejectAuthenticator: (error: any) => void) => {
                    let result = authenticator(req, res, next, def);

                    result.then((resultAuth: boolean) => {
                        if (!authenticated) {
                            authenticated = resultAuth;
                        }
                        resolveAuthenticator();
                    }).catch(rejectAuthenticator);
                });
            }).then(() => {
                resolve(authenticated);
            }).catch(reject);
        });
    }
}