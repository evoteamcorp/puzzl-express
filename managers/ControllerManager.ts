import {
    IManager,
    LoggerCore,
    FileUtil,
    PathsUtil,
    PackageJsonCore,
    PieceManager,
    EventManager,
    LoaderCore,
    Instantiate,
    MetaObject,
    Injector
} from "puzzl-core/Exports";
import {WatchManager} from "puzzl-file-watcher/Exports";
import {PathDef} from "../manual_typings/built_in";
import fs = require("fs");
import path = require("path");
import {Express} from "express";
import {BlueprintManager} from "./BlueprintManager";
import {RouteManager} from "./RouteManager";
import {AbstractController} from "../controllers/AbstractController";

@Instantiate()
@MetaObject()
@Injector()
export class ControllerManager implements IManager {

    // List of controller paths
    private _controllerPaths: Array<PathDef>            = [];
    private _controllersStructs: Object                 = {};
    private _moduleName: string                         = "Express";
    private _watcherPackageName: string                 = "puzzl-file-watcher";
    private _watcherModuleName: string                  = "FileWatcher";
    private _express: Express                           = null;

    constructor(
        private _logger: LoggerCore,
        private _fileUtil: FileUtil,
        private _pathsUtil: PathsUtil,
        private _packageJsonCore: PackageJsonCore,
        private _pieceManager: PieceManager,
        private _eventManager: EventManager,
        private _watchManager: WatchManager,
        private _loaderCore: LoaderCore,
        private _blueprintManager: BlueprintManager,
        private _routeManager: RouteManager
    ) {
    }

    /**
     * Set the express module
     * @param express
     */
    public setExpress(express: Express): void {
        this._express = express;
    }

    /**
     * Register all controllers
     * @returns {Promise<any>}
     */
    public load(): Promise<any> {
        this._logger.debug("Express Controller manager load...");
        let loadControllers = (resolve: (value?: any) => void, reject: (error: any) => void): Promise<any> => {
            let promisePath = (path: PathDef): Promise<any> => {
                this._logger.debug("Load controllers in path ", path.path);
                let resolveFilesInDir = (...args: Array<any>): Promise<any> => {
                    args.push(path.path);
                    args.push(path.moduleName);
                    return this._loadControllers.apply(this, args);
                };

                return this._fileUtil.dirExist(path.path).then(
                    (result: boolean) => {
                        return new Promise(
                            (resolveCtrlPath: (value?: any) => void, rejectCtrlPath: (error: any) => void) => {
                                if (result) {
                                    this._fileUtil.getFilesInDir(path.path)
                                        .then(resolveFilesInDir).then(resolveCtrlPath).catch(rejectCtrlPath);
                                } else {
                                    this._logger.warn("Controller directory:", path.path, "does not exist...");
                                    resolveCtrlPath();
                                }
                            }
                        );
                    }
                );
            };

            return Promise.each(this._controllerPaths, promisePath).then(resolve).catch(reject);
        };

        let promiseLoadControllersInExpress = (): Promise<any> => {
            return this._loadControllersInExpress();
        };

        return new Promise(loadControllers)
            .then(promiseLoadControllersInExpress);
    }

    /**
     * Initialize
     * @returns {Promise<T>}
     */
    public initialize(): Promise<any> {
        return new Promise(
            (resolve: Function): void => {
                this.addControllerPath(this._pathsUtil.getPiecePath(this._moduleName, "controllers"), this._moduleName);
                this.addControllerPath(this._pathsUtil.getApp("controllers"), "App");
                this._logger.debug("List of controller paths :", this._controllerPaths);
                resolve();
            }
        );
    }

    /**
     * Add a controller path
     * @param path
     * @param moduleName
     */
    public addControllerPath(path: string, moduleName: string): void {
        this._logger.debug("Add controller path :", path);
        this._controllerPaths.push(
            {
                moduleName: moduleName,
                path      : path
            }
        );

        this._createWatcher(path, moduleName);
    }

    /**
     * Return controller
     * @param controllerName
     * @param moduleName
     * @returns {any}
     */
    public getController(controllerName: string, moduleName: string): void {
        return this._controllersStructs[moduleName][controllerName];
    }

    /**
     * Return the list of controllers
     * @param moduleName
     * @returns {string[]}
     */
    public getControllerList(moduleName?: string): Array<string> {
        let controllerList = [];
        if (moduleName && moduleName in this._controllersStructs) {
            controllerList = Object.keys(this._controllersStructs[moduleName]);
        } else {
            for (let key in this._controllersStructs) {
                if (this._controllersStructs.hasOwnProperty(key)) {
                    controllerList = controllerList.concat(Object.keys(this._controllersStructs[key]));
                }
            }
        }

        return controllerList;
    }

    /**
     * Create a watcher
     * @param path
     * @param moduleName
     * @private
     */
    private _createWatcher(path: string, moduleName: string): void {
        if (this._packageJsonCore.checkPieceIn(this._watcherPackageName)) {

            let registerWatchPath = () => {
                this._watchManager.addWatchPath(
                    {
                        callback : (eventType: string, file?: string, stat?: fs.Stats): void => {
                            this._reloadControllers(eventType, file, stat, moduleName);
                        },
                        eventType: ["add", "change", "unlink"],
                        path     : path
                    }
                );
            };

            if (this._pieceManager.isLoaded(this._watcherModuleName)) {
                registerWatchPath();
            } else {
                this._eventManager.on(
                    "module:" + this._watcherModuleName + ":initialized",
                    registerWatchPath
                );
            }
        } else {
            this._logger.debug(this._watcherModuleName + " is not present");
        }
    }

    /**
     * Register blueprint controller
     * @returns {Promise}
     * @private
     */
    private _registerBlueprintControllers(): Promise<any> {
        return this._blueprintManager.load();
    }

    /**
     * Reload a controller
     * @param eventType
     * @param file
     * @param stat
     * @param moduleName
     * @private
     */
    private _reloadControllers(eventType: string, file?: string, stat?: fs.Stats, moduleName?: string): void {
        this._logger.debug("Watch event:", eventType, file);
        if (eventType === "change") {
            this._loadController(path.basename(file), path.dirname(file), moduleName);
        }
    }

    /**
     * Load list of controllers
     * @param files
     * @param path
     * @param moduleName
     * @return {Promise<string[]>}
     */
    private _loadControllers(files: Array<string>, path: string, moduleName: string): Promise<any> {
        this._logger.debug("Controller list ", files);
        let promiseEach = (file: string): Promise<any> => {
            return this._loadController(file, path, moduleName);
        };

        return Promise.each(files, promiseEach);
    }

    /**
     * Load a controller
     * @param file
     * @param path
     * @param moduleName
     * @returns {Promise<any>}
     * @private
     */
    private _loadController(file: string, path: string, moduleName: string): Promise<any> {
        this._logger.debug("Load controller", file, "in module", moduleName);
        return this._loaderCore.require(
            path + "/" + file, moduleName, (...args: Array<any>) => {
                return this._registerController.apply(this, args);
            }
        );
    }

    /**
     * Register a controller
     * @param name
     * @param moduleName
     * @param object
     * @returns {Promise<T>|Promise}
     * @private
     */
    private _registerController(name: string, moduleName: string, object: Object): Promise<any> {
        let promiseRegisterController = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            try {
                this._logger.debug("Register controller", name);
                if (!(
                        moduleName in this._controllersStructs
                    )) {
                    this._controllersStructs[moduleName] = {};
                }
                if (name in this._controllersStructs[moduleName] && this._controllersStructs[moduleName][name]) {
                    this._logger.debug("Controller", name, "already registered extend existing controller with the newest one");
                    (object as typeof AbstractController).extend(this._controllersStructs[moduleName][name]);
                }

                this._controllersStructs[moduleName][name] = object;
                resolve();
            } catch (e) {
                reject(e);
            }
        };

        return new Promise(promiseRegisterController);
    }

    /**
     * Load controllers in express instance
     * @returns {any}
     * @private
     */
    private _loadControllersInExpress(): Promise<any> {
        this._logger.debug("Load controllers", Object.keys(this._controllersStructs), "in express");
        let promiseLoadBlueprints = () => {
            return this._registerBlueprintControllers();
        };

        return this._loadRoutes().then(promiseLoadBlueprints);
    }

    /**
     * Load routes
     * @returns {Promise<T>|Promise}
     * @private
     */
    private _loadRoutes(): Promise<any> {
        this._routeManager.setExpress(this._express);
        return this._routeManager.load();
    }
}