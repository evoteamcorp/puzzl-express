/**
 * Created by acidspike on 14/06/2016.
 */
import {Instantiate, MetaObject, Injector, IManager, ConfigsCore, FileUtil, PathsUtil, LoggerCore, StringUtil} from "puzzl-core/Exports";
import {AuthManager} from "./AuthManager";
import {Request} from "express";
import {Response} from "express";
import {NextFunction} from "express";
import {ControllerManager} from "./ControllerManager";
import {ErrorExpress} from "../helpers/ErrorExpress";
import {RouteManager} from "./RouteManager";
import {BlueprintController} from "../controllers/BlueprintController";

@Instantiate()
@MetaObject()
@Injector()
export class BlueprintManager implements IManager {

    private _models: Array<string> = [];
    private _moduleName: string    = "Express";

    constructor(
        private _coreConfigs: ConfigsCore,
        private _fileUtil: FileUtil,
        private _pathsUtil: PathsUtil,
        private _logger: LoggerCore,
        private _stringUtil: StringUtil,
        private _routeManager: RouteManager,
        private _authManager: AuthManager,
        private _controllerManager: ControllerManager
    ) {
    }

    /**
     * Initialize blueprint
     * @returns {any}
     */
    public initialize(): Promise<any> {
        let blueprintPath = this._coreConfigs.get("Express").paths.blueprint;
        blueprintPath     = this._pathsUtil.getApp(blueprintPath);

        return this._fileUtil.dirExist(blueprintPath).then(
            (result: boolean) => {
                return new Promise(
                    (resolve: (value?: any) => void, reject: (error: any) => any) => {
                        if (result) {
                            this._fileUtil.getFilesInDir(blueprintPath).then(
                                (files: Array<string>) => {
                                    this._logger.debug("Detected models for blueprints:", files);
                                    files.forEach(
                                        (file: string) => {
                                            this._models.push(this._stringUtil.uncamelize(file).replace("-model", ""));
                                        }
                                    );
                                    resolve();
                                }
                            ).catch(reject);
                        } else {
                            this._logger.warn("Unable to activate blueprint, the path does not exists:", blueprintPath);
                            resolve();
                        }
                    }
                );
            }
        );
    }

    /**
     * Load all blueprints routes
     * @returns {any}
     */
    public load(): Promise<any> {
        return Promise.each(
            this._models, (model: string) => {
                return new Promise(
                    (resolve: () => void, reject: (error: any) => void) => {
                        this._routeManager.bindRoute(
                            ["get", "post", "put", "delete"], "/" + model + "/:id?", (req: Request, res: Response, next: NextFunction) => {
                                this._handleBlueprintRequest(req, res, next);
                            }
                        ).then(resolve).catch(reject);
                    }
                );
            }
        );
    }

    /**
     * Handle a blueprint request
     * @param req
     * @param res
     * @param next
     * @private
     */
    private _handleBlueprintRequest(req: Request, res: Response, next: NextFunction): void {
        this._authManager.checkIfAuthorized(req, res, next).then(
            (authorized: boolean) => {
                return new Promise(
                    (resolve: (value?: any) => void, reject: (error: any) => void) => {
                        if (req.xhr || this._coreConfigs.get("CoreConfig").debug) {
                            if (authorized) {
                                let ctrl: BlueprintController = new this._controllerManager.getController("Blueprint", this._moduleName)(
                                    req,
                                    res,
                                    next,
                                    "Blueprint",
                                    "rest"
                                );
                                ctrl.rest().then(resolve).catch(reject);
                            } else {
                                let err        = new ErrorExpress(req, "Auth.Error.NeedAuthenticationOrAuthorization");
                                err.statusCode = 401;
                                reject(err);
                            }
                        } else {
                            let err        = new ErrorExpress(req, "Http.Error.ScaffoldingNotImplementedYet");
                            err.statusCode = 400;
                            reject(err);
                        }
                    }
                );
            }
        ).then(
            (datas: any) => {
                res.json(datas);
            }
        ).catch(next);
    }
}