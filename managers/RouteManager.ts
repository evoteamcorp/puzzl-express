/**
 * Created by acidspike on 14/06/2016.
 */
import {IManager, Injector, MetaObject, Instantiate, ConfigsCore, LoggerCore} from "puzzl-core/Exports";
import {Express, Request, Response, NextFunction} from "express";
import {WaitingRouteDictionary, RouteDef} from "../manual_typings/built_in";
import {AuthManager} from "./AuthManager";
import {ErrorExpress} from "../helpers/ErrorExpress";
import {ControllerManager} from "./ControllerManager";
import {AbstractController} from "../controllers/AbstractController";

@Instantiate()
@MetaObject()
@Injector()
export class RouteManager implements IManager {

    private _express: Express = null;
    private _waitingRoutes: WaitingRouteDictionary;

    constructor(
        private _configsCore: ConfigsCore,
        private _logger: LoggerCore,
        private _authManager: AuthManager,
        private _controllerManager: ControllerManager
    ) {
    }

    /**
     * Initialize route manager
     * @returns {Promise}
     */
    public initialize(): Promise<any> {
        return new Promise(
            (resolve: () => void) => {
                resolve();
            }
        );
    }

    /**
     * Set express instance
     * @param express
     */
    public setExpress(express: Express): void {
        this._express = express;
    }

    public load(): Promise<any> {
        let routes = this._configsCore.get("Routes");

        let promiseLoadRoutes = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            try {
                if (routes) {
                    let matchers = Object.keys(routes);

                    Promise.each(
                        matchers, (matcher: string): Promise<any> => {
                            return this._loadRoute(matcher, routes[matcher]);
                        }
                    ).then(resolve);
                } else {
                    resolve();
                }
            } catch (e) {
                reject(e);
            }
        };

        return new Promise(promiseLoadRoutes);
    }

    /**
     * Bind a route in express
     * @param method
     * @param route
     * @param routeHandler
     * @param noWaitingRoutesCheck
     * @returns {Promise}
     */
    public bindRoute(
        method: string|Array<string>,
        route: string,
        routeHandler: (req: Request, res: Response, next: NextFunction) => void,
        noWaitingRoutesCheck: boolean = false
    ): Promise<any> {
        return new Promise(
            (resolve: () => void, reject: (error: any) => void) => {
                try {
                    if (typeof method === "string") {
                        method = [method as string];
                    }

                    if (!noWaitingRoutesCheck) {
                        this._checkWaitingRoutes();
                    }

                    (
                        method as Array<string>
                    ).forEach(
                        (methodName: string) => {
                            if (this._express) {
                                this._express[methodName](route, routeHandler);
                            } else {
                                this._waitingRoutes[methodName]        = this._waitingRoutes[methodName] || {};
                                this._waitingRoutes[methodName][route] = routeHandler;
                            }
                        }
                    );
                    resolve();
                } catch (e) {
                    reject(e);
                }
            }
        );
    }

    /**
     * Handle waiting routes to bind
     * @private
     */
    private _checkWaitingRoutes(): void {
        if (this._express) {
            Object.keys(this._waitingRoutes).forEach(
                (method: string) => {
                    if (this._waitingRoutes.hasOwnProperty(method)) {
                        let routes = this._waitingRoutes[method];
                        Object.keys(routes).forEach(
                            (route: string) => {
                                if (routes.hasOwnProperty(route)) {
                                    this.bindRoute(method, route, routes[route], true);
                                }
                            }
                        );
                    }
                }
            );
        }
    }

    /**
     * Load a route
     * @param matcher
     * @param def
     * @returns {Promise}
     * @private
     */
    private _loadRoute(matcher: string, def: RouteDef): Promise<any> {
        let promiseLoadRoute = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            try {
                let method = "get";
                let path   = matcher;
                if (/\s/.test(matcher)) {
                    let res = matcher.split(/\s/);
                    method  = res[0];
                    path    = res[1];
                }

                if (!def.action && !def.view) {
                    def.action = "index";
                }

                this._logger.debug("Load route", method, path, def);
                let methods: Array<string> = method.split("|");

                this.bindRoute(
                    methods, path, (req: Request, res: Response, next: NextFunction) => {
                        this._handleRequest(req, res, next, def);
                    }
                );

                resolve();
            } catch (e) {
                reject(e);
            }
        };

        return new Promise(promiseLoadRoute);
    }

    /**
     * Handle an http request
     * @param req
     * @param res
     * @param next
     * @param def
     * @private
     */
    private _handleRequest(req: Request, res: Response, next: NextFunction, def: RouteDef): void {
        let p: Promise<boolean> = null;
        if (def.auth) {
            p = this._authManager.checkIfAuthentication(req, res, next, def);
        } else if (def.needAuth || def.needAuth === undefined) {
            p = this._authManager.checkIfAuthorized(req, res, next, def);
        } else {
            p = new Promise(
                (resolve: (value: boolean) => void) => {
                    resolve(true);
                }
            );
        }

        p.then(
            (authorized: boolean) => {
                if (authorized) {
                    let ctrl: AbstractController = this._getController(req, res, next, def);
                    if (ctrl) {
                        this._playAction(ctrl, def, req, next);
                    }
                } else {
                    if (def.needAuthRedirect) {
                        req.flash("error", "Auth.Error.NeedAuthenticationOrAuthorization");
                        res.redirect(def.needAuthRedirect);
                    } else {
                        let err        = new ErrorExpress(req, "Auth.Error.NeedAuthenticationOrAuthorization");
                        err.statusCode = 401;
                        next(err);
                    }
                }
            }
        );
    }

    /**
     * Return the controller matching request
     * @param req
     * @param res
     * @param next
     * @param def
     * @returns {null}
     * @private
     */
    private _getController(req: Request, res: Response, next: NextFunction, def: RouteDef): AbstractController {
        let ctrl = this._controllerManager.getController(def.controller, def.moduleName || "App");
        if (ctrl) {
            return new this._controllerManager.getController(def.controller, def.moduleName || "App")(
                req,
                res,
                next,
                def.controller,
                def.action
            );
        } else {
            let err        = new ErrorExpress(req, "Http.Error.NoController", {controller: def.controller});
            err.statusCode = 404;
            next(err);
            return null;
        }
    }

    /**
     * Call the request action in given controller
     * @param ctrl
     * @param def
     * @param req
     * @param next
     * @private
     */
    private _playAction(ctrl: AbstractController, def: RouteDef, req: Request, next: NextFunction): void {
        if (def.action) {
            if (typeof ctrl[def.action] === "function") {
                let result = ctrl[def.action]();
                this._handleOutput(ctrl, result);
            } else {
                let err        = new ErrorExpress(req, "Http.Error.NoControllerAction", {action: def.action, controller: def.controller});
                err.statusCode = 404;
                next(err);
            }
        } else {
            ctrl.sendHtml(def.view);
        }
    }

    /**
     * Handle the output stream
     * @param ctrl
     * @param result
     * @private
     */
    private _handleOutput(ctrl: AbstractController, result: any): void {
        if (result && typeof result.then === "function") {
            result.then(
                (datas: any) => {
                    this._sendOutput(ctrl, datas);
                }
            );
        } else if (result) {
            this._sendOutput(ctrl, result);
        }
    }

    /**
     * Send the output stream
     * @param ctrl
     * @param result
     * @private
     */
    private _sendOutput(ctrl: AbstractController, result: any): void {
        if (ctrl.isXhr() && ctrl.accept(["json", "html"], "json")) {
            ctrl.sendJson(result);
        } else {
            ctrl.sendHtml(result);
        }
    }
}