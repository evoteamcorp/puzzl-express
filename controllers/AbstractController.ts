/**
 * Created by acidspike on 14/06/2016.
 */
import {Response} from "express";
import {Request} from "express";
import {NextFunction} from "express";
import {ErrorExpress} from "../helpers/ErrorExpress";

export class AbstractController {

    protected res: Response;
    protected req: Request;
    protected next: NextFunction;
    protected name: string;
    protected action: string;
    protected templateEngine: string = "ejs";

    /**
     * Base controller constructor
     * @param req
     * @param res
     * @param next
     * @param name
     * @param action
     */
    constructor(req: Request, res: Response, next: NextFunction, name: string, action: string) {
        this.req    = req;
        this.res    = res;
        this.next   = next;
        this.name   = name;
        this.action = action;
    }

    /**
     * Method for extend a controller for full js code support
     * @param ctrl
     * @param proto
     */
    public static extend(ctrl: any, proto?: Object): any {
        for (let property in this) {
            if (this.hasOwnProperty(property)) {
                ctrl[property] = this[property];
            }
        }

        function __(): void {
            this.constructor = ctrl;
        }

        ctrl.prototype = this === null ? Object.create(this) : (__.prototype = this.prototype, new __());
        ctrl._super = this;

        if (proto) {
            global._.merge(ctrl.prototype, proto);
        }
    }

    /**
     * Extend prototype
     * @param proto
     */
    public static setProto(proto: Object): void {
        if (proto) {
            global._.merge(this.prototype, proto);
        }
    }

    /**
     * Send json datas
     * @param args
     */
    public sendJson(...args: Array<any>): void {
        this.res.json.apply(this.res, args);
    }

    /**
     * Render template of action
     * @param file
     * @param datas
     * @param callback
     */
    public sendHtml(file?: string|Object, datas?: Object|Function, callback?: (err: Error, html: string) => void): void {
        if (typeof datas === "function") {
            callback = datas as (err: Error, html: string) => void;
            datas    = undefined;
        }

        if (typeof file !== "string") {
            datas = file;
            file  = this.name + "/" + this.action + "." + this.templateEngine;
        } else {
            file = this.name + "/" + file + "." + this.templateEngine;
        }

        this.res.render(file as string, datas, callback);
    }

    /**
     * Send a file to download
     * @param args
     */
    public sendFile(...args: Array<string>): void {
        this.res.download.apply(this.res, args);
    }

    /**
     * Return if the type is accepted
     * @param type
     * @param filteredType
     * @returns {boolean}
     */
    public accept(type: string|string[], filteredType?: string): boolean {
        if (filteredType) {
            return this.req.accepts(type as Array<string>) === filteredType;
        } else {
            return !!this.req.accepts(type as Array<string>);
        }
    }

    /**
     * Return if the request is an xhr request
     * @returns {any}
     */
    public isXhr(): boolean {
        return this.req.xhr;
    }

    /**
     * Set the response header field (call of this function will flush previous setted headers)
     * @param args
     */
    protected setHeader(...args: Array<any>): void {
        this.res.set.apply(this.res, args);
    }

    /**
     * Set the response header field (call of this function will flush previous setted headers)
     * @param args
     */
    protected setHeaders(...args: Array<string>): void {
        this.setHeaders.apply(this, args);
    }

    /**
     * Add a header to the response
     * @param args
     */
    protected addHeader(...args: Array<string>): void {
        this.res.append.apply(this.res, args);
    }

    /**
     * Return value of given header
     * @param args
     * @returns {any}
     */
    protected getHeader(...args: Array<any>): string {
        return this.req.get.apply(this.req, args);
    }

    /**
     * Return value of paramName or null
     * @param paramName
     * @returns {any}
     */
    protected getParam(paramName: string): any {
        if (this.req.params && paramName in this.req.params) {
            return this.req.params[paramName];
        } else {
            return null;
        }
    }

    /**
     * Return all params
     * @returns {any}
     */
    protected getAllParams(): Object {
        return this.req.params;
    }

    /**
     * Return value of queryParamName or null
     * @param queryParamName
     * @returns {any}
     */
    protected getQuery(queryParamName: string): any {
        if (this.req.query && queryParamName in this.req.query) {
            return this.req.query[queryParamName];
        } else {
            return null;
        }
    }

    /**
     * Return all query params
     * @returns {any}
     */
    protected getAllQuery(): Object {
        return this.req.query;
    }

    /**
     * Return body datas
     * @returns {any}
     */
    protected getBodyDatas(): Object {
        return this.req.body;
    }

    /**
     * Return value of bodyParamName or null
     * @param bodyParamName
     * @returns {any}
     */
    protected getBodyData(bodyParamName: string): any {
        if (this.req.body && bodyParamName in this.req.body) {
            return this.req.body[bodyParamName];
        } else {
            return null;
        }
    }

    /**
     * Return the http method
     * @returns {any}
     */
    protected getMethod(): string {
        return this.req.method;
    }

    /**
     * Return base url
     * @returns {any}
     */
    protected getBaseUrl(): string {
        return this.req.baseUrl;
    }

    /**
     * Return the original url
     * @returns {any}
     */
    protected getOriginalUrl(): string {
        return this.req.originalUrl;
    }

    /**
     * Return the url
     * @returns {any}
     */
    protected getUrl(): string {
        return this.req.url;
    }

    /**
     * Return the path of the url
     * @returns {any}
     */
    protected getPath(): string {
        return this.req.path;
    }

    /**
     * Return value of cookieName or null
     * @param cookieName
     * @param signed
     * @returns {any}
     */
    protected getCookie(cookieName: string, signed: boolean = false): string {
        let store = (
                        signed
                    ) ? this.req.signedCookies : this.req.cookies;
        if (cookieName in store) {
            return store[cookieName];
        } else {
            return null;
        }
    }

    /**
     * Return all cookies
     * @param signed
     * @returns {any}
     */
    protected getAllCookies(signed: boolean = false): Object {
        return (
                   signed
               ) ? this.req.signedCookies : this.req.cookies;
    }

    /**
     * Return if the request has cache control
     * @returns {any}
     */
    protected isFreshRequest(): boolean {
        return this.req.fresh;
    }

    /**
     * Return hostname
     * @returns {any}
     */
    protected getHostname(): string {
        return this.req.hostname;
    }

    /**
     * Return the client ip
     * @returns {any}
     */
    protected getClientIp(): string {
        return this.req.ip;
    }

    /**
     * Return ips of the client if it pass through proxy and if trust proxy is not to false
     * @returns {any}
     */
    protected getClientIps(): Array<string> {
        return this.req.ips;
    }

    /**
     * Return the current protocol
     * @returns {any}
     */
    protected getProtocol(): string {
        return this.req.protocol;
    }

    /**
     * Return the current matching route
     * @returns {any}
     */
    protected getCurrentRoute(): Object {
        return this.req.route;
    }

    /**
     * Return if the current request was made over https
     * @returns {any}
     */
    protected isHttps(): boolean {
        return this.req.secure;
    }

    /**
     * Return current subdomains
     * @returns {any}
     */
    protected getSubdomains(): Array<string> {
        return this.req.subdomains;
    }

    /**
     * Throw an error with status code
     * @param statusCode
     * @param message
     */
    protected throwError(statusCode: Number, message: string): void {
        let err        = new ErrorExpress(this.req, message);
        err.statusCode = statusCode;
        throw err;
    }

    /**
     * Redirect to url
     * @param url
     * @param statusCode
     */
    protected redirect(url: string, statusCode?: number): void {
        if (statusCode) {
            this.res.redirect(statusCode, url);
        } else {
            this.res.redirect(url);
        }
    }
}
