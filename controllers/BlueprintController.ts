import {AbstractController} from "./AbstractController";
import {Request} from "express";
import {Response} from "express";
import {NextFunction} from "express";
import {ErrorExpress} from "../helpers/ErrorExpress";

export class BlueprintController extends AbstractController {

    constructor(req: Request, res: Response, next: NextFunction, name: string, action: string) {
        super(req, res, next, name, action);
    }

    /**
     * Rest entry point
     * @return {Promise<any>}
     */
    public rest(): Promise<any> {
        let method = this.getMethod().toLowerCase();
        return new Promise((resolve: (value: any) => void, reject: (error: any) => void) => {
            if (method === "get") {
                if (this.getParam("id")) {
                    this._findOne().then(resolve).catch(reject);
                } else {
                    this._find().then(resolve).catch(reject);
                }
            } else if (method === "post") {
                this._create().then(resolve).catch(reject);
            } else if (method === "put" || method === "patch") {
                if (this.getParam("id")) {
                    this._update().then(resolve).catch(reject);
                } else {
                    let err = new ErrorExpress(this.req, "Blueprint.Error.NoIdGiven");
                    err.statusCode = 400;
                    reject(err);
                }
            } else if (method === "delete") {
                if (this.getParam("id")) {
                    this._remove().then(resolve).catch(reject);
                } else {
                    let err = new ErrorExpress(this.req, "Blueprint.Error.NoIdGiven");
                    err.statusCode = 400;
                    reject(err);
                }
            }
        });
    }

    /**
     * Find one object of model
     * @private
     * @return {any}
     */
    private _findOne(): Promise<any> {
        return (
            <OrmManager>ModularApp.getManager("Orm", "ModularOrm")
        ).findOne(
            this.getParam("model"),
            this.getParam("id")
        );
    }

    /**
     * Find all object of model
     * @return {number}
     * @private
     */
    private _find(): Promise<any> {
        return (
            <OrmManager>ModularApp.getManager("Orm", "ModularOrm")
        ).find(this.getParam("model"));
    }

    /**
     * Create an entry of model
     * @return {Domain|*|HTMLImageElement|HTMLOptionElement|XMLHttpRequest}
     * @private
     */
    private _create(): Promise<any> {
        return (
            <OrmManager>ModularApp.getManager("Orm", "ModularOrm")
        ).create(
            this.getParam("model"),
            this.getBodyDatas()
        );
    }

    /**
     * Update an entry of model
     * @returns {void|IDBRequest|Hash|Hmac|Buffer|string}
     * @private
     */
    private _update(): Promise<any> {
        return (
            <OrmManager>ModularApp.getManager("Orm", "ModularOrm")
        ).update(
            this.getParam("model"),
            this.getParam("id"),
            this.getBodyDatas()
        );
    }

    /**
     * Delete an entry of model
     * @return {Promise<any>}
     * @private
     */
    private _remove(): Promise<any> {
        return (
            <OrmManager>ModularApp.getManager("Orm", "ModularOrm")
        ).remove(
            this.getParam("model"),
            this.getParam("id")
        );
    }
}
